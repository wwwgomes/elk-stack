package com.wwwgomes.elk.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ElkStackController {

	private static final Logger LOG = Logger.getLogger(ElkStackController.class.getName());

	@Autowired
	private RestTemplate rest;

	@RequestMapping(value = "/elkdemo")
	public String helloUser() {
		String response = "Hello user!" + LocalDate.now();
		LOG.log(Level.INFO, "/elkdemo - &gt; {}", response);

		return response;
	}

	@RequestMapping(value = "/elk")
	public String helloElk() {
		String response = (String) rest
				.exchange("http://localhost:8080/elkdemo", HttpMethod.GET, null, new ParameterizedTypeReference() {
				}).getBody();
		LOG.log(Level.INFO, "/elk - &gt; {}", response);

		try {
			String exceptionrsp = (String) rest.exchange("http://localhost:8080/exception", HttpMethod.GET, null,
					new ParameterizedTypeReference() {
					}).getBody();
			LOG.log(Level.INFO, "/elk trying to print exception - &gt; {}", exceptionrsp);
			response = response + " === " + exceptionrsp;
		} catch (Exception e) {
			// TODO: handle exception
		}

		return response;

	}

	@RequestMapping(value = "/exception")
	public String exception() {
		String rsp = "";
		try {
			int i = 1 / 0;
			// should get exception
		} catch (Exception e) {
			e.printStackTrace();
			LOG.log(Level.SEVERE, "{}", e);

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString(); // stack trace as a string
			LOG.log(Level.SEVERE, "Exception As String :: - &gt; " + sStackTrace);

			rsp = sStackTrace;
		}

		return rsp;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
